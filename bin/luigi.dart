import 'character.dart';

class Luigi extends Character {
  Luigi(String name, int tall, String color, int size)
      : super(name, tall, color, size);

  @override
  String getName() {
    return name;
  }

  @override
  int getTall() {
    return tall;
  }

  @override
  void eatmushrooms() {
    print("Luigi : $name  eatmushrooms!!");
  }

  void nameToString() {
    print("Luigi : $name");
  }

  void tallToString() {
    print("Luigi : $tall cm");
  }

  int sum = 0;
  int count = 0;

  void tallCheck(int tallcheck) {
    count = tall - tallcheck;
    sum = tallcheck - tall;

    if (tall < tallcheck) {
      print("Luigi less tall than $sum cm");
    } else {
      print("Luigi greater tall than $count cm");
    }
  }
}
