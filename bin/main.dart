import 'luigi.dart';
import 'mario.dart';

void main() {
  Mario mario = Mario("mario", 140, "red", 50);
  Luigi luigi = Luigi("luigi", 130, "green", 38);

  mario.eatmushrooms();
  mario.tallToString();
  mario.tallCheck(luigi.tall);
  mario.jump();
  print("___________________\n");

  luigi.eatmushrooms();
  luigi.tallToString();
  luigi.tallCheck(mario.tall);
  luigi.jump();
}
