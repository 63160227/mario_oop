class Character {
  String name = "";
  int tall = 0;
  String color = "";
  int size = 0;

  Character(this.name, this.tall, this.color, this.size);
  void jump() {
    print("Mario and Luigi jump!!");
  }

  void eatmushrooms() {
    print("Mario and Luigi eatmushrooms");
    print("Name: $name Color: $color Size: $size Tall: $tall");
  }

  String getName() {
    return name;
  }

  int getTall() {
    return tall;
  }

  String getColor() {
    return color;
  }

  int getSize() {
    return size;
  }
}
