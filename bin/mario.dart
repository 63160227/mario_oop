import 'character.dart';

class Mario extends Character {
  Mario(String name, int tall, String color, int size)
      : super(name, tall, color, size);

  void Name() {
    print("Mario : $name");
  }

  @override
  String getName() {
    return name;
  }

  @override
  int getTall() {
    return tall;
  }

  @override
  void eatmushrooms() {
    print("Mario : $name  eatmushrooms!!");
  }

  void nameToString() {
    print("Mario : $name");
  }

  void tallToString() {
    print("Mario : $tall cm");
  }

  int sum = 0;
  int count = 0;

  void tallCheck(int tallcheck) {
    sum = tallcheck - tall;
    count = tall - tallcheck;

    if (tall < tallcheck) {
      print("Mario less tall than $sum cm");
    } else {
      print("Mario greater tall than $count cm");
    }
  }

  @override
  void jump() {
    print("Mario $name jump up !!");
  }
}
